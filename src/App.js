import React, { Component } from 'react';
import Header from './component/layout/Header';
import Contacts from './component/contact/Contacts';
import AddContact from './component/contact/AddContact';
import { Provider } from './context';

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

class App extends Component {
  render() {
    return (
      <Provider>
        <div className="App">
          <Header branding="Contact Manager"/>
          <div className="container">
          <AddContact />
          <Contacts />
          </div>         
      </div>
      </Provider>
    );
  }
}

export default App;
