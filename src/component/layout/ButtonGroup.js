import React from 'react';
import PropTypes from 'prop-types';

const ButtonGroup = ({
    type,
    value
}) => {
  return (
    <input
        type="submit"
        value ="Add Contact"
        className="btn btn-light btn-block"
    ></input>
  )
};

ButtonGroup.propTypes = {
    type : PropTypes.string.isRequired,
    value : PropTypes.string.isRequired,
  };

export default ButtonGroup;
