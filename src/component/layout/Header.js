import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Header extends Component {
  render() {
      const {branding} = this.props;
    return (
      <div>
        <nav className="navbar navbar-expand-sm navbar-dark bg-danger mb-3 py-0">
            <div className="container">
                <a className="navbar-brand" href="/">{branding}</a>
                <div>
                    <ul className="navbar-nav">
                        <li className="nav-item"><a className="nav-link" href="/">Home</a></li>
                        <li className="nav-item"><a className="nav-link" href="/">Home</a></li>
                        <li className="nav-item"><a className="nav-link" href="/">Home</a></li>
                    </ul>
                </div>
            </div>
        </nav>
      </div>
    )
  }
};


Header.propTypes = {
    branding : PropTypes.string.isRequired
};

export default Header;
