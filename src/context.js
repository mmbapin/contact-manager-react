import React, {Component} from 'react';

const Context = React.createContext();

/////Reducer Pass all action work flow///////

const reducer = (state, action) => {
    switch(action.type) {
        case "DELETE_CONTACT" :
            return {
                ...state,
                contacts : state.contacts.filter(
                    contact => contact.id !== action.payload
                )
            };

        case "ADD_CONTACT" :
            return {
                ...state,
                contacts : [action.payload, ...state.contacts]
            };

        default :
            return state;    
    }
};

/////This is Basically the state . That means this is our global state ////////
export class Provider extends Component {
    state = {
        contacts : [
            {
                id : 1,
                name : "M.M.Bapin",
                email : "bapinmm@gmail.com",
                phone : "555-5555-555"
            },
            {
                id : 2,
                name : "M.M.Bapin",
                email : "bapinmm@gmail.com",
                phone : "555-5555-555"
            },
            {
                id : 3,
                name : "M.M.Bapin",
                email : "bapinmm@gmail.com",
                phone : "555-5555-555"
            }
        ],

        ////by using dispatch we need to call reducer and action
        dispatch : action => this.setState(state => reducer(state, action))
    };

    render () {
        return (
            <Context.Provider value={this.state}>
                {this.props.children}
            </Context.Provider>
        )
    }

};

export const Consumer = Context.Consumer;